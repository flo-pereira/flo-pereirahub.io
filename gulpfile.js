'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    bourbon = require('node-neat'),
    livereload = require('gulp-livereload'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    streamify = require('gulp-streamify'),
    uglify = require('gulp-uglify'),
    reactify = require('reactify');

gulp.task('scripts', function () {
  return browserify({
        entries: ['./src/js/app'],
        transform: [reactify],
        debug:true
    }).bundle()
    .pipe(source('app.min.js'))
    //.pipe(streamify(uglify()))
    .pipe(gulp.dest('./build/js/'))
    .pipe(livereload());
});

gulp.task('styles', function () {
    return gulp.src('./src/scss/*.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: ['./src/scss/']
            .concat(bourbon.includePaths, 'node_module/font-awesome/scss/')
        }))
        .pipe(gulp.dest('./build/css'))
        .pipe(livereload());
});

gulp.task('default', ['styles', 'scripts'], function() {
    livereload.listen();
    gulp.watch('src/scss/**', ['styles']);
    gulp.watch('src/js/**', ['scripts']);
});