var React = require('react'),
	FromField = require('./fromField.jsx'),
	MsgField = require('./msgField.jsx'),
	SendBtn = require('./sendBtn.jsx'),
	$ = require('superagent');

module.exports = React.createClass({

	getDefaultProps: function () {
		return {
			me: "pereira.florian@gmail.com"
		};
	},

	getInitialState: function () {
		return {
			from: null,
			msg: null,
			btn: SendBtn.conf()
		};
	},

	setFrom: function (from) {
		this.setState({
			from: from
		});
	},

	setMsg: function (msg) {
		this.setState({
			msg: msg
		});
	},

	send: function (e) {
		e.preventDefault();
		var self = this;
		var data = {
			"key": "4WTaKp15xCbaFMLZAamchw",
			"message": {
				"from_email": this.state.from,
				"to": [{
					"email": this.props.me,
	                "name": "Florian Pereira",
	                "type": "to"
	            }],
	            "subject": "¡Hola!",
	            "html": this.state.msg
	        }
		};

		$.post('https://mandrillapp.com/api/1.0/messages/send.json')
			.send(data)
		  .end(function(error, res) {
		  	if (res.status !== 200) {
				self.setState({
					btn: {
						className: 'error',
						label: 'Please use your own mail client'
					}
				});

    			window.location.href = ['mailto:', self.props.me, '?subject=contact from ', self.state.from, '&body=', self.state.msg].join('');
		  	} else {
			  	self.setState({
			  		btn: {
						className: 'success',
						label: 'Your message has been sent'
					}
				});
			}
		  });
	},

	render: function () {
		return (
			<form onSubmit={this.send}>
				<div className="to">
					<label>To</label> 
					<div className="myMail">{this.props.me}</div>
				</div>
				<FromField value={this.state.from} handleChange={this.setFrom} />
				<MsgField  value={this.state.msg} handleChange={this.setMsg}/>
				<SendBtn conf={this.state.btn}/>
			</form>
		);
	}
});