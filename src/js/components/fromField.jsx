var React = require('react');

module.exports = React.createClass({
	handleChange: function (e) {
		this.props.handleChange(e.target.value);
	},

	render: function () {
		return (
			<div className="from">
				<label>From</label>
				<input type="email" required value={this.props.value} onChange={this.handleChange}/>
			</div>
		);
	}
});