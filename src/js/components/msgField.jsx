var React = require('react');

module.exports = React.createClass({
	handleChange: function (e) {
		this.props.handleChange(e.target.value);
	},

	render: function () {
		return (
			<div className="msg">
				<textarea required onChange={this.handleChange}>{this.props.value}</textarea>
			</div>
		);
	}
});