var React = require('react'),
		$ = require('superagent');

module.exports = React.createClass({

	statics: {
		conf: function() {
			return this.getDefaultProps().conf;
		}
	},

	getDefaultProps: function () {
		return {
			conf: {
				className: 'default',
				label: 'Send'
			}
		};
	},

	shouldComponentUpdate: function (nextProps) {
		return nextProps.conf !== this.props.conf;
	},

	render: function () {
		return (
			<div className={["button", this.props.conf.className].join(' ')}>
				<button type="submit">{this.props.conf.label}</button>
			</div>
		);
	}
});