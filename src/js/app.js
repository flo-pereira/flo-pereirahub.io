var React = require('react'),
	contactForm = React.createFactory(require('./components/contactForm.jsx'));

React.render(
  contactForm(),
  document.getElementById('contactForm')
);